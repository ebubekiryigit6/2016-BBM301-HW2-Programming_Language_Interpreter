digit [0-9]
integer [-+]?[0-9]+
float [0-9]+"."[0-9]+
string "\""{alphanumeric}*"\""
alphabetic [A-Za-z]
upperCase [A-Z]
alphanumeric ({alphabetic}|{digit})
variable {alphabetic}{alphanumeric}*
operations "+"|"-"|"*"|"/"|"%"
number {float}|{integer}|{digit}


%{


#include <stdio.h>
#include <string.h>
#include "y.tab.h"
int line_count=0;


%}


%%




"while" {return (WHILE);}
"xLoop" {return (XLOOP);}
"continue" {return (CONTINUE);}
"break" {return(BREAK);}
"print" {return(PRINT);}


"if" { return(IF); }
"else" { return(ELSE); }
"elif" { return(ELIF); }
"switch" { return(SWITCH); }
"case" { return(CASE); }
"default" { return(DEFAULT); }


"ArrayList" {return(ARRAYLIST);}
"HashMap" {return(HASHMAP);}
"LinkedList" {return(LINKEDLIST);}
"Matrix" {return(MATRIX);}


"TRUE"          {return(TRUE);}
"FALSE"         {return(FALSE);}


"return" {return(RETURN);}



"="  { return(ART_EQUALS); }
"+"  { return(PLUS); }
"-"  { return(SUB); }
"*"  { return(TIMES); }
"/"  { return(DIVIDE); }
"%"  { return(MOD); }

"+="  { return(PLUS_EQUALS); }
"-="  { return(SUB_EQUALS); }
"*="  { return(TIMES_EQUALS); }
"/="  { return(DIVIDE_EQUALS); }
"%="  { return(MOD_EQUALS); }


"."	{ return(DOT); }
";"	{ return(SemCLN); }
","	{ return(COMMA); }
":"	{ return(COLON); }
"("	{ return(LEFTPARANTHESIS); } 
")"	{ return(RIGHTPARANTHESIS); }
"["	{ return(LEFTBRACKET); } 
"]"	{ return(RIGHTBRACKET); }
"{"	{ return(LCURLYBRACKET); } 
"}"	{ return(RCURLYBRACKET); }
".||."    { return(COMMENT); }



"&&" { return(AND); }
"||" { return(OR); }
"!" { return(NOT); }
"===" { return(LOG_EQUALS); }
"^" { return(XOR); }


">=" { return(GREAT_EQUALS); }
"<=" { return(LESS_EQUALS); }
">" { return(GREATER); }
"<" { return(LESS); }




{variable} {return(VARIABLE);}
{number} {return (NUMBER);}
{string} {return (STRING);}


[\t]	    { ; }
[\r\n]	    {line_count++;}
[ \t\v\f]   { }




%%




int yywrap(){
	return 1;
}