%{
#include <stdio.h>

extern char *yytext;
extern int line_count;

int yylex(void);
void yyerror(const char *);

%}



%token WHILE XLOOP CONTINUE BREAK PRINT
%token LEFTBRACKET RIGHTBRACKET LCURLYBRACKET RCURLYBRACKET COMMENT
%token AND OR NOT LOG_EQUALS XOR
%token GREAT_EQUALS LESS_EQUALS GREATER LESS
%token TRUE FALSE
%token ART_EQUALS PLUS SUB TIMES DIVIDE MOD
%token COMMA COLON DOT SemCLN
%token LEFTPARANTHESIS RIGHTPARANTHESIS
%token PLUS_EQUALS SUB_EQUALS TIMES_EQUALS DIVIDE_EQUALS MOD_EQUALS
%token IF ELSE ELIF SWITCH CASE DEFAULT
%token ARRAYLIST HASHMAP LINKEDLIST MATRIX
%token RETURN VARIABLE
%token NUMBER STRING


%start program;



%%

program
	: _function_
	| program _function_
	;


_function_	
	: VARIABLE LEFTPARANTHESIS parameters RIGHTPARANTHESIS function_block
	;


function_block
	: LCURLYBRACKET statements RCURLYBRACKET		
	;


parameters
	:
	| VARIABLE		
	| parameters COMMA VARIABLE 
        	
	;


statements
	: statement SemCLN
	| statements statement SemCLN		
	;


statement
	: assign_statement		
	| control_statement
        | loop_statement
	| function_statement
	| return_statement
	| print_statement
	| arraylist_declaration
	| hashmap_declaration
	| linkedlist_declaration
	| matrix_declaration	
	;


assign_statement
	: VARIABLE ART_EQUALS right_assign
	| VARIABLE shortcut_operations right_assign
	| 
	;

print_statement
	: PRINT VARIABLE

operations
	: PLUS
	| SUB
	| TIMES
	| DIVIDE 
	| MOD	
	;

shortcut_operations
	: PLUS_EQUALS 
	|SUB_EQUALS 
	|TIMES_EQUALS 
	|DIVIDE_EQUALS 
	|MOD_EQUALS
	;


right_assign
	: VARIABLE
	| VARIABLE operations variable_list
	| STRING
	| NUMBER
	| STRING operations variable_list
	| NUMBER operations variable_list
	;

variable_list
	: VARIABLE
	| VARIABLE operations right_assign
	| STRING
	| NUMBER
	| STRING operations right_assign
	| NUMBER operations right_assign
	;


control_statement
	: if_statement
	| switch_statement
	;


if_statement
	: IF LEFTPARANTHESIS boolean_exp RIGHTPARANTHESIS function_block
	| if_statement ELIF LEFTPARANTHESIS boolean_exp RIGHTPARANTHESIS function_block
	| if_statement ELSE function_block
	;


switch_statement
	: SWITCH LEFTPARANTHESIS VARIABLE RIGHTPARANTHESIS function_block
	| SWITCH LEFTPARANTHESIS VARIABLE RIGHTPARANTHESIS LCURLYBRACKET switch_cases RCURLYBRACKET
	;


switch_cases
	: CASE boolean_exp COLON statements
	| CASE boolean_exp COLON statements BREAK
	| CASE boolean_exp COLON statements switch_cases
	| CASE boolean_exp COLON statements BREAK switch_cases
	| default
	;


default
	: DEFAULT COLON statements BREAK
	| DEFAULT COLON statements 
	;


loop_statement
	: while_loop
	| xLoop
	;

loop_block 
	: LCURLYBRACKET statements RCURLYBRACKET
	;

while_loop
	: WHILE LEFTPARANTHESIS boolean_exp RIGHTPARANTHESIS loop_block
	;


xLoop
	: XLOOP LEFTPARANTHESIS VARIABLE SemCLN boolean_exp SemCLN VARIABLE shortcut_operations right_assign RIGHTPARANTHESIS loop_block
	;



function_statement
	: VARIABLE LEFTPARANTHESIS parameter_list RIGHTPARANTHESIS
	;


parameter_list
	: empty
	| right_assign
	| right_assign COMMA parameter_list
	;


return_statement
	: RETURN right_assign
	;


boolean_exp
	: TRUE
	| FALSE
	| VARIABLE
	| logical_exp
	| NUMBER
	;


logical_exp
	: boolean_exp boolean_check boolean_exp
	| boolean_exp relational_check boolean_exp
	;


boolean_check
	: AND
	| OR
	| XOR
	;


relational_check
	: LESS
	| LESS_EQUALS
	| LOG_EQUALS
	| GREATER
	| GREAT_EQUALS
	;

arraylist_declaration
	: ARRAYLIST VARIABLE ART_EQUALS ARRAYLIST LEFTPARANTHESIS RIGHTPARANTHESIS
	;
hashmap_declaration
	: HASHMAP VARIABLE ART_EQUALS HASHMAP LEFTPARANTHESIS RIGHTPARANTHESIS
	;
linkedlist_declaration
	: LINKEDLIST VARIABLE ART_EQUALS LINKEDLIST LEFTPARANTHESIS RIGHTPARANTHESIS
	;
matrix_declaration
	: MATRIX VARIABLE ART_EQUALS MATRIX LEFTBRACKET RIGHTBRACKET LEFTBRACKET RIGHTBRACKET


empty
	: 
	;

%%

void yyerror(const char *s){
	fprintf(stderr,"<%s>,INVALID RULE IN AVENGER LANGUAGE line : %d\n",s,line_count);
}
int main(){

	if( yyparse() )
		printf("\n Error is found during parsing process.\n\n");
         else
               printf("Parsing completed.SUCCESSFULLY!\n\n");
	
	
	return 0;
}
