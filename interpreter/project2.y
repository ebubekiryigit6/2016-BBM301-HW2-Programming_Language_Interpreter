%{
void yyerror (char *s);
#include <stdio.h>    
#include <stdlib.h>
int symbols[52];
int symbolVal(char symbol);
void second_value(char symbol, int val);
%}

%union {int num; char id;}         
%start line
%token print_function
%token exit_command
%token <num> number
%token <id> identifier
%type <num> line exp term fact 
%type <id> assignment
%token ART_EQUALS
%left PLUS SUB 
%left TIMES DIVIDE




%%


line    : assignment ';'		{;}
		| exit_command ';'		{exit(EXIT_SUCCESS);}
		| print_function exp ';'			{printf("Printing %d\n", $2);}
		| line assignment ';'	{;}
		| line print_function exp ';'	{printf("Printing %d\n", $3);}
		| line exit_command ';'	{exit(EXIT_SUCCESS);}
        ;


assignment : identifier ART_EQUALS exp  { second_value($1,$3); }
			;
exp    	: fact                  {$$ = $1;}
       	| exp PLUS fact         {$$ = $1 + $3;}
       	| exp SUB fact          {$$ = $1 - $3;}
	;

fact    :term			{$$ = $1;}
	| exp TIMES term        {$$ = $1 * $3;}
	| exp DIVIDE term       {$$ = $1 / $3;}
	| term TIMES term        {$$ = $1 * $3;}
	| term DIVIDE term       {$$ = $1 / $3;}
       	;

term   	: number                {$$ = $1;}
		| identifier	{$$ = value($1);} 
        ;


%%                   


int index_compute(char parameter)
{
	int control = -1;
	if(islower(parameter)) {
		control = parameter - 'a' + 26;
	} else if(isupper(parameter)) {
		control = parameter - 'A';
	}
	return control;
} 



int value(char symbol)
{
	int temp = index_compute(symbol);
	return symbols[temp];
}



void second_value(char symbol, int val)
{
	int temp = index_compute(symbol);
	symbols[temp] = val;
}


int main (int argc, char **argv) {
	int i;
	for(i=0; i<52; i++) {
		symbols[i] = 0;
	}

	return yyparse ( );
}


void yyerror (char *s) {fprintf (stderr, "%s\n", s);} 






