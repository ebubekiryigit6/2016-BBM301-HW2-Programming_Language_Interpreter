%{
#include "y.tab.h"
%}
%%


"="  				   { return(ART_EQUALS); }
"+"  				   { return(PLUS); }
"-"     			   { return(SUB); }
"*"        			   { return(TIMES); }
"/"  			  	   { return(DIVIDE); }
"print"		  		   {return print_function;}
"exit"				   {return exit_command;}
[a-zA-Z]+			   {yylval.id = yytext[0]; return identifier;}
[0-9]+                		   {yylval.num = atoi(yytext); return number;}
[ \t\n]                            ;
[-+=;]           	   	   {return yytext[0];}
.                                  {return ;}


%%
int yywrap (void) {return 1;}
